package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	dc "github.com/docker/cli/cli/compose/types"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"gitlab.com/accumulatenetwork/accumulate/config"
	cfg "gitlab.com/accumulatenetwork/accumulate/config"
	"gitlab.com/accumulatenetwork/accumulate/internal/logging"
	"gitlab.com/accumulatenetwork/accumulate/internal/node"
	"gitlab.com/accumulatenetwork/accumulate/networks"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

type Node struct {
	IP   string          `json:"ip"`
	Type config.NodeType `json:"type"`
}
type Subnet struct {
	Name  string             `json:"name"`
	Type  config.NetworkType `json:"type"`
	Port  int                `json:"port"`
	Nodes []Node             `json:"nodes"`
}

type Network struct {
	Network string   `json:"network"`
	Subnet  []Subnet `json:"subnet"`
}

func loadNetworkConfiguration(file string) (ret Network, err error) {
	jsonFile, err := os.Open(file)
	defer func() { _ = jsonFile.Close() }()
	// if we os.Open returns an error then handle it
	if err != nil {
		return ret, err
	}
	data, _ := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(data, &ret)
	return ret, err
}

//load network config file
func initNetwork(cmd *cobra.Command, args []string) {

	if cmd.Flag("network").Changed {
		fatalf("--network is not applicable to accumulated init validator")
	}

	networkConfigFile := args[0]
	network, err := loadNetworkConfiguration(networkConfigFile)
	check(err)

	var directory *Subnet
	var bvns []*Subnet

	var bvnSubnet []*Subnet

	//now look for the subnet.
	for i, v := range network.Subnet {
		//while we are at it, also find the directory.
		if v.Type == config.Directory {
			if directory != nil {
				fatalf("more than one directory subnet is defined, can only have 1")
			}
			directory = &network.Subnet[i]
		}
		if v.Type == config.BlockValidator {
			bvnSubnet = append(bvnSubnet, &network.Subnet[i])
		}
	}

	if directory == nil {
		fatalf("cannot find directory configuration in %v", networkConfigFile)
		panic("not reached") // For static analysis
	}

	if directory.Name != "Directory" {
		fatalf("directory name specified in file was %s, but accumulated requires it to be \"Directory\"", directory.Name)
	}
	//quick validation to make sure the directory node maps to each of the BVN's defined
	for _, dnn := range directory.Nodes {
		found := false
		for _, bvn := range network.Subnet {
			if bvn.Type == config.Directory {
				continue
			}
			for _, v := range bvn.Nodes {
				if strings.EqualFold(dnn.IP, v.IP) {
					bvns = append(bvns, &bvn)
					found = true
					break
				}
			}
			if found {
				break
			}
		}
		if !found {
			fatalf("%s is defined in the directory nodes networks file, but has no supporting BVN node", dnn.IP)
		}
	}

	// we'll need 1 DN for each BVN.
	numBvns := len(network.Subnet) - 1

	if flagInitNetwork.Compose {
		flagInitNetwork.Docker = true
	}

	count := 1 //we will only need a count of 1 since the bvn and dn will be run in the same app
	compose := new(dc.Config)
	compose.Version = "3"
	compose.Services = make([]dc.ServiceConfig, 0, 1+count*(numBvns+1))
	compose.Volumes = make(map[string]dc.VolumeConfig, 1+count*(numBvns+1))

	switch len(flagInitDevnet.IPs) {
	case 1:
		// Generate a sequence from the base IP
	case count * (numBvns + 1):
		// One IP per node
	default:
		fatalf("not enough IPs - you must specify one base IP or one IP for each node")
	}

	addresses := make(map[string][]string, len(directory.Nodes))
	dnConfig := make([]*cfg.Config, len(directory.Nodes))
	var dnRemote []string
	dnListen := make([]string, len(directory.Nodes))

	var accSub []config.Subnet
	for _, sub := range network.Subnet {
		s := config.Subnet{}
		s.ID = sub.Name
		s.Type = sub.Type
		for _, a := range sub.Nodes {
			address := fmt.Sprintf("http://%s:%d", a.IP, sub.Port)
			n := config.Node{}
			n.Address = address
			n.Type = a.Type
			s.Nodes = append(s.Nodes, n)
		}
		accSub = append(accSub, s)
	}

	//need to configure the dn for each BVN assuming 1 bvn
	for i := range directory.Nodes {
		var remotes []string
		dnConfig[i], remotes, dnListen[i] = initNetworkNode(network.Network, directory.Name, directory.Nodes, cfg.Directory,
			cfg.Validator, i, i, compose)

		dnRemote = append(dnRemote, remotes...)
		if flagInitNetwork.Docker && flagInitNetwork.DnsSuffix != "" {
			for j := range dnRemote {
				dnRemote[j] += flagInitNetwork.DnsSuffix
			}
		}
		c := dnConfig[i]
		if flagInit.NoEmptyBlocks {
			c.Consensus.CreateEmptyBlocks = false
		}

		if flagInit.NoWebsite {
			c.Accumulate.Website.Enabled = false
		}
		dnListen[i] = "0.0.0.0"
		c.Accumulate.Network.LocalSubnetID = directory.Name
		c.Accumulate.Network.Type = directory.Type
		c.Accumulate.Network.Subnets = accSub
		c.Accumulate.Network.LocalAddress = fmt.Sprintf("%s:%d", bvns[i].Nodes[0].IP, directory.Port)
	}

	bvnConfig := make([][]*cfg.Config, len(bvnSubnet))
	bvnListen := make([][]string, len(bvnSubnet))
	for i, v := range bvnSubnet {
		bvnConfig[i] = make([]*cfg.Config, len(v.Nodes))
		bvnListen[i] = make([]string, len(v.Nodes))
	}

	bvnRemote := make([][]string, len(bvnSubnet)) //numBvns)

	for i, v := range bvnSubnet {
		for j := range v.Nodes {
			bvnConfig[i][j], bvnRemote[i], bvnListen[i][j] = initNetworkNode(network.Network, v.Name, v.Nodes, cfg.BlockValidator,
				cfg.Validator, i, j, compose)
			if flagInitNetwork.Docker && flagInitNetwork.DnsSuffix != "" {
				for j := range bvnRemote[i] {
					bvnRemote[i][j] += flagInitNetwork.DnsSuffix
				}
			}
			c := bvnConfig[i][j]
			if flagInit.NoEmptyBlocks {
				c.Consensus.CreateEmptyBlocks = false
			}

			if flagInit.NoWebsite {
				c.Accumulate.Website.Enabled = false
			}
			c.Accumulate.Network.Type = config.BlockValidator
			c.Accumulate.Network.LocalSubnetID = v.Name
			c.Accumulate.Network.LocalAddress = fmt.Sprintf("%s:%d", v.Nodes[j].IP, v.Port)
			c.Accumulate.Network.Subnets = accSub
		}
	}

	for _, sub := range bvnSubnet {
		for _, bvn := range sub.Nodes {
			addresses[sub.Name] = append(addresses[sub.Name], fmt.Sprintf("http://%s:%d", bvn.IP, sub.Port))
		}
	}

	for i, v := range bvnSubnet {
		for j := range v.Nodes {
			c := bvnConfig[i][j]
			if flagInit.NoEmptyBlocks {
				c.Consensus.CreateEmptyBlocks = false
			}

			if flagInit.NoWebsite {
				c.Accumulate.Website.Enabled = false
			}
			c.Accumulate.Network.Type = config.BlockValidator
			c.Accumulate.Network.LocalSubnetID = v.Name
			c.Accumulate.Network.LocalAddress = fmt.Sprintf("%s:%d", v.Nodes[j].IP, v.Port)
			c.Accumulate.Network.Subnets = accSub
		}
	}

	if flagInit.Reset {
		nodeReset()
	}

	if !flagInitNetwork.Compose {
		logger := newLogger()
		check(node.Init(node.InitOptions{
			WorkDir:  filepath.Join(flagMain.WorkDir, "dn"),
			Port:     directory.Port,
			Config:   dnConfig,
			RemoteIP: dnRemote,
			ListenIP: dnListen,
			Logger:   logger.With("subnet", protocol.Directory),
		}))

		for i := range bvnSubnet {
			check(node.Init(node.InitOptions{
				WorkDir:  filepath.Join(flagMain.WorkDir, fmt.Sprintf("bvn%d", i)),
				Port:     bvns[i].Port,
				Config:   bvnConfig[i],
				RemoteIP: bvnRemote[i],
				ListenIP: bvnListen[i],
				Logger:   logger.With("subnet", bvns[i].Name),
			}))
		}
		return
	}

	var svc dc.ServiceConfig
	api := fmt.Sprintf("http://%s:%d/v2", dnRemote[0], flagInitDevnet.BasePort+networks.AccRouterJsonPortOffset)
	svc.Name = "tools"
	svc.ContainerName = "devnet-init"
	svc.Image = flagInitDevnet.DockerImage
	svc.Environment = map[string]*string{"ACC_API": &api}

	svc.Command = dc.ShellCommand{"init", "devnet", "-w", "/nodes", "--docker"}

	cmd.Flags().Visit(func(flag *pflag.Flag) {
		switch flag.Name {
		case "work-dir", "docker", "compose", "reset":
			return
		}

		s := fmt.Sprintf("--%s=%v", flag.Name, flag.Value)
		svc.Command = append(svc.Command, s)
	})

	if flagInitDevnet.UseVolumes {
		svc.Volumes = make([]dc.ServiceVolumeConfig, len(compose.Services))
		for i, node := range compose.Services {
			bits := strings.SplitN(node.Name, "-", 2)
			svc.Volumes[i] = dc.ServiceVolumeConfig{Type: "volume", Source: node.Name, Target: path.Join("/nodes", bits[0], "Node"+bits[1])}
		}
	} else {
		svc.Volumes = []dc.ServiceVolumeConfig{
			{Type: "bind", Source: ".", Target: "/nodes"},
		}
	}

	compose.Services = append(compose.Services, svc)

	dn0svc := compose.Services[0]
	dn0svc.Ports = make([]dc.ServicePortConfig, networks.MaxPortOffset+1)
	for i := range dn0svc.Ports {
		port := uint32(flagInitDevnet.BasePort + i)
		dn0svc.Ports[i] = dc.ServicePortConfig{
			Mode: "host", Protocol: "tcp", Target: port, Published: port,
		}
	}

	f, err := os.Create(filepath.Join(flagMain.WorkDir, "docker-compose.yml"))
	check(err)
	defer f.Close()

	//err = yaml.NewEncoder(f).Encode(compose)
	check(err)

	//	initValidatorNode("dn", dnBasePort, cmd, args)
}

func initNetworkNode(networkName string, subnetName string, nodes []Node, netType cfg.NetworkType, nodeType cfg.NodeType, bvn, node int, compose *dc.Config) (config *cfg.Config, remote []string, listen string) {
	if netType == cfg.Directory {
		config = cfg.Default(netType, nodeType, protocol.Directory)
	} else {
		config = cfg.Default(netType, nodeType, fmt.Sprintf("BVN%d", bvn))
	}
	if flagInit.LogLevels != "" {
		_, _, err := logging.ParseLogLevel(flagInit.LogLevels, io.Discard)
		checkf(err, "--log-level")
		config.LogLevel = flagInit.LogLevels
	}

	if flagInit.NoEmptyBlocks {
		config.Consensus.CreateEmptyBlocks = false
	}
	if flagInit.NoWebsite {
		config.Accumulate.Website.Enabled = false
	}

	var remotes []string
	if !flagInitNetwork.Docker {
		for _, v := range nodes {
			remotes = append(remotes, v.IP)
		}
		//need to find the bvn that mates the current
		return config, remotes, "0.0.0.0"
	}

	var name, dir string
	if netType == cfg.Directory {
		name, dir = fmt.Sprintf("dn-%d", node), fmt.Sprintf("./dn/Node%d", node)
	} else {
		name, dir = fmt.Sprintf("bvn%d-%d", bvn, node), fmt.Sprintf("./%s/Node%d", strings.ToLower(subnetName), node)
	}

	var svc dc.ServiceConfig
	svc.Name = name
	svc.ContainerName = networkName + "-" + name
	svc.Image = flagInitDevnet.DockerImage
	svc.DependsOn = []string{"tools"}

	if flagInitDevnet.UseVolumes {
		svc.Volumes = []dc.ServiceVolumeConfig{
			{Type: "volume", Source: name, Target: "/node"},
		}
		compose.Volumes[name] = dc.VolumeConfig{}
	} else {
		svc.Volumes = []dc.ServiceVolumeConfig{
			{Type: "bind", Source: dir, Target: "/node"},
		}
	}

	compose.Services = append(compose.Services, svc)
	remotes = append(remotes, svc.Name)
	return config, remotes, "0.0.0.0"
}
